import { GET_PRODUCTS, GET_PRODUCTS_SUCCESS, GET_PRODUCTS_FAILURE } from '../actions/productsActions';

const initialState = {
  status: 'NOT_PENDING',
  error: 0
}

export default function products(state = initialState, action) {
  switch(action.type) {

    case GET_PRODUCTS: 
       return {
         ...state,
         status: 'IN_PROGRESS'
       }

    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        status: 'SUCCESS',
        data: [...action.data]
      }

    case GET_PRODUCTS_FAILURE:
      return {
        ...state,
        status: 'ERROR',
        error: 4010
      }

    default: return state;
  }
}
