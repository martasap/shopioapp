import React from 'react';
import { Link } from 'react-router-dom';
import '../../static/styles/components/ProductList.css';

function ProductList({ products = {} }) {
  return (
    <div className="product-list">
      {products.map((product) => {
        return (
          <Link 
            key={product.id}
            to={`/${product.id}`} className="product-list-single">
            <img src={`${product.img_url}`} alt={`${product.name}`}/>
            <div>
              <h4>{product.name}</h4>
              <p>{product.price}</p>
            </div>
          </Link>
        );
      })}
    </div>
  )
}

export default ProductList;