export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const ADD_TO_BASKET_SUCCESS = 'ADD_TO_BASKET_SUCCESS';
export const ADD_TO_BASKET_FAILURE = 'ADD_TO_BASKET_FAILURE';

function addToBasket(selectedProduct) {
  return {
    type: 'ADD_TO_BASKET',
    selectedProduct
  }
}

function addToBasketSuccess(selectedProduct) {
  return {
    type: 'ADD_TO_BASKET_SUCCESS',
    selectedProduct
  }
}

  function addToBasketFailure(selectedProduct) {
    return {
      type: 'ADD_TO_BASKET_FAILURE',
      selectedProduct
    }
  }

  export function addToBasketRequest(selectedProduct) {
    return function (dispatch) {
      dispatch(addToBasket(selectedProduct));

      return Promise.resolve(true)
        .then(
          response => response,
          error => dispatch(addToBasketFailure(selectedProduct))
        )
        .then(response => {
          dispatch(addToBasketSuccess(selectedProduct));
        });
    }
  }