import React from 'react';
import { connect } from 'react-redux';
import { addToBasketRequest } from '../actions/basketActions';
import ProductList from '../components/ProductList';
import EmptyProductList from '../components/EmptyProductList';
import ErrorPage from '../components/Error';
import SearchBar from '../components/SearchBar';
import Loader from '../components/Loader';
import '../../static/styles/containers/ProductListContainer.css';

class ProductListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { filteredProducts: null }
    this.searchProducts = this.searchProducts.bind(this);
  }

  searchProducts(inputText = '') {
    const { data } = this.props.products;
    const filteredProducts = data.filter((product) => {
      return product.name.toLowerCase().includes(inputText.toLowerCase());
    });
    return this.setState({filteredProducts: filteredProducts});
  }

  render() {
    const { products } = this.props;
    const { filteredProducts } = this.state;
    return (
      <div className="product-list-container"> 
        <SearchBar searchProducts={this.searchProducts} />
        {
          products.status === ('IN_PROGRESS' || 'NOT_PENDING')
          ? <Loader/>
          : products.status === 'ERROR'
            ? <ErrorPage/>
            : (products.data.length) 
              ? <ProductList products={filteredProducts ? filteredProducts : products.data} />
              : <EmptyProductList />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.products,
    basket: state.basket
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToBasket: (selectedProduct) => {
      dispatch(addToBasketRequest(selectedProduct));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListContainer);
