import React from 'react';
import '../../static/styles/components/SearchBar.css';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.searchProducts(event.target.value);
        this.setState({value: event.target.value});
    }

    render() {
        return (
            <div className="search-bar">
                <input type="text" value={this.state.value} onChange={this.handleChange}/>
            </div>
        )
    }
}

export default SearchBar;