import React from 'react';
import { connect } from 'react-redux';

function ShoppingBasket (props) {
    const { basket } = props;
    return <span className="shopping-basket">ShoppingBasket: {basket.items.length}</span>
}

const mapStateToProps = (state) => {
    return {
      basket: state.basket
    }
  }

  export default connect(mapStateToProps)(ShoppingBasket);
