import React from 'react';
import { connect } from 'react-redux';
import ErrorPage from '../components/Error';
import Loader from '../components/Loader';
import { addToBasketRequest } from '../actions/basketActions';
import '../../static/styles/components/SingleProductPage.css';

class SingleProductPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {size: ''};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({size: event.target.value});
  }

  render() {
  const requestStatuses = ['NOT_PENDING','IN_PROGRESS'];
  const { products, product, addToBasket } = this.props;
  const selectedProduct = {
    ...product, selectedSize: this.state.size
  }
  return requestStatuses.includes(products.status)
    ? <Loader /> 
    : (products.status) === 'ERROR' || !product
      ? <ErrorPage /> 
      : <div className="single-product-page">
          <img src={`${product.img_url}`} alt={`${product.name}`}/>
          <div className="single-product-data">
            <div className="single-product-properties">
              <div>{product.name}</div>
              <div>{product.price}</div>
            </div>
            <div className="single-product-form-elements">
              <select value={this.state.size} onChange={this.handleChange}>
              <option value="">Choose size:</option>
                {
                  product.sizes.map((size)=>{
                    return <option key={size} value={size}>{size}</option>;
                  })
                }
              </select>
              <button onClick={() => addToBasket(selectedProduct)} disabled={!this.state.size}>Add to basket</button>
            </div>
        </div>
      </div>
  }
}

const mapStateToProps = (state, props) => {
  const { match: { params } } = props;
  const { products } = state
  const product = (products.data || []).find((item, index)=>{return item.id == params.productId});

  return {
    products: state.products,
    product: product
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToBasket: (selectedProduct) => {
      dispatch(addToBasketRequest(selectedProduct));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleProductPage);


