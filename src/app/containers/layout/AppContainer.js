import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { getProductsRequest } from '../../actions/productsActions';
import ProductListContainer from '../ProductListContainer';
import AppHeader from '../../components/AppHeader';
import SingleProductPage from '../../components/SingleProductPage';
import '../../../static/styles/containers/AppContainer.css';

 class AppContainer extends React.Component {
  componentWillMount () {
    this.props.getProducts();
  }
  render () {
    return (
      <Router>
        <div className="app-container">
          <AppHeader /> 
          <Route path="/:productId/" exact component={SingleProductPage}/> 
          <Route path="" exact component={ProductListContainer}/> 
        </div>
      </Router>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.products
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProducts: () => {
      dispatch(getProductsRequest());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);