import React from 'react';
import '../../static/styles/components/AppHeader.css';
import ShoppingBasket from './ShoppingBasket';

function AppHeader() {
    return (
      <header className="app-header">
        <span>Shopio</span>
        <span>Products</span>
        <ShoppingBasket />
      </header>
  )
}

export default AppHeader;