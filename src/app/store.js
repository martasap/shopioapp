import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import productsReducer from './reducers/productsReducer';
import basketReducer from './reducers/basketReducer';
import { combineReducers } from 'redux';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(combineReducers({
  products: productsReducer,
  basket: basketReducer}),
  composeEnhancers(
    applyMiddleware(thunkMiddleware)
  )
);

export default store;