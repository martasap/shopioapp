import fakeProductData from '../constants/fakeProductData.json';
export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';

export function getProductsRequest() {
  return function (dispatch) {
    dispatch(getProducts());

    return Promise.resolve(fakeProductData)
    .then(
      response => response,
      error => dispatch(getProductsFailure())
    )
    .then(response => dispatch(getProductsSuccess(response)));
  }
}

export function getProducts() {
  return {
    type: 'GET_PRODUCTS'
  }
}

function getProductsSuccess(productsList) {
  return {
    type: 'GET_PRODUCTS_SUCCESS',
    data: productsList
  }
}

function getProductsFailure() {
  return {
    type: 'GET_PRODUCTS_FAILURE'
  }
}