import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './app/store';
import AppContainer from './app/containers/layout/AppContainer';


class App extends Component {
  render() {
    return(
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}

render(<App />, document.getElementById('root'));
