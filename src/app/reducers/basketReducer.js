import { ADD_TO_BASKET, ADD_TO_BASKET_SUCCESS, ADD_TO_BASKET_FAILURE } from '../actions/basketActions';
  
  const initialState = {
    items: [],
    status: 'NOT_PENDING',
    error: 0
  }

  export default function products(state = initialState, action) {
    switch(action.type) {
      
      case ADD_TO_BASKET: 
         return {
           ...state,
           status: 'IN_PROGRESS'
         }
  
      case ADD_TO_BASKET_SUCCESS:
        return {
           ...state,
           status: 'SUCCESS',
           items: [...state.items, action.selectedProduct]
        }
  
      case ADD_TO_BASKET_FAILURE:
        return {
          ...state,
          status: 'ERROR',
          error: 4000
        }
    
      default: return state;
    }
  }
  
