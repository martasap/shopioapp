import React from 'react';

function EmptyProductList() {
  return (
    <div className="empty-product-list-wrapper">
      <div className="empty-product-list">
        <h2>There are no products</h2>
      </div>
    </div>
  )
}

export default EmptyProductList;